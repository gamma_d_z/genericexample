public class Protector<T> {

    private T protectedObject;
    private int passwordHash;

    public void protectObject(T toProtect,String password){
        protectedObject = toProtect;
        passwordHash = password.hashCode();
        System.out.println(passwordHash);
    }

    public T getObject(String password){
        if(password.hashCode() == passwordHash){
            return  protectedObject;
        }else {
            return null;
        }
    }
}
