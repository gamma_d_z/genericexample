public class Student {
    String name;
    int score;

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public Student(String serilized){
        this.name = serilized.split(" ")[0];
        this.score = Integer.parseInt(serilized.split(" ")[1]);
    }

    @Override
    public String toString() {
        return name + " " + score;
    }
}
