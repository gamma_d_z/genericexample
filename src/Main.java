import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Student s = new Student("sadra" , 12);
        Protector<Student> t = new Protector<Student>();
        t.protectObject(s,"ghazanfar");
        System.out.println(t.getObject("ghazanfar").name);
        Teacher teacher =new Teacher("gholam", 48);
        Protector<Teacher> pt = new Protector<Teacher>();
        pt.protectObject(teacher,"khale ghezi");

    }
}